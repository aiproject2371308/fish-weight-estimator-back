from typing import Union

from fastapi import FastAPI
from pydantic import BaseModel
import joblib
from fastapi.middleware.cors import CORSMiddleware
import numpy as np

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


model = joblib.load('Fish_Weight_Estimator.pkl')


class InputData (BaseModel):
    specie : int
    length1 : float
    length2 : float
    length3 : float
    height : float
    width : float

@app.post("/predict")
def predict(data: InputData):
    features = [[data.specie, data.length1, data.length2, data.length3, data.height, data.width]]
    prediction = model.predict(features)
    formatted_prediction = round(prediction[0], 2)
    return formatted_prediction

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}